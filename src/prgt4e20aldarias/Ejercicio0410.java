/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt4e20aldarias;

/**
 * Fichero: Ejercicio0410.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 18-nov-2013
 */
public class Ejercicio0410 {

  public static void main(String[] args) {
    for (int i = 2; i < 101; i += 2) {
      System.out.print(i + " ");
    }
  }
}
