package prgt4e20aldarias;

/**
 * Fichero: Ejercicio0412.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 18-nov-2013
 */
public class Ejercicio0412 {

  public static void main(String args[]) {
    int contador = 0;
    while (contador < 100) {
      contador++;
      if (contador % 2 != 0) {
        continue;
      }
      System.out.println(contador + "");
      if (contador >= 7) {
        break;
      }
    }
  }
}
