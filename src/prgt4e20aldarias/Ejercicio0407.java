/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package prgt4e20aldarias;

/**
 * Fichero: Ejercicio0407.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */

public class Ejercicio0407 {

  public static void main(String [] args) {
    for (int i=1; i< 11 ; i++) {
      System.out.println("Tabla del "+i);
      System.out.println("****************");
      for (int j=1; j<11; j++) {
        System.out.println(i+" x "+j+" = "+i*j);
      }
      System.out.println("");
    }
  }
}